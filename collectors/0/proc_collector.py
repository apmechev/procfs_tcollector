#!/usr/bin/env python
# NTP Offset stats
# charlesrg AT gmail.com
#
# procstat.py
#

import os
import subprocess
import sys
import time
import errno
import Queue
from threading import Thread

import pdb
from collectors.lib import utils

class flushfile(file):
    def __init__(self, f):
        self.f = f
    def write(self, x):
        self.f.write(x)
        self.f.flush()



try:
  from collectors.etc import proc_conf
except ImportError:
  proc_conf = None

DEFAULT_COLLECTION_INTERVAL=0.5
collection_interval=DEFAULT_COLLECTION_INTERVAL
q=Queue.Queue()

def getPIDs(st):
    ''' This searches the current user's running processes for a string
        which allows to trace scripts by searching the srcipt name whereas
        pidof will only catch the interpreter name. Uses ps.
    '''
    USER=os.environ["USER"]
    pl = subprocess.Popen(['ps', '-u',USER,'-o','pid cmd'], stdout=subprocess.PIPE).communicate()[0]
    ##TODO:Wrap above in trycatch
    pids=[]

    for i in pl.split('\n'):
        if st in i:
            pids.append(i.lstrip().split(' ')[0])

    return pids

def printer(p):
    while p.poll()==None:
        for line in iter(p.stdout.readline, b''):
            sys.stdout.write(line.rstrip()+'\n')
        sys.stdout.flush()

def queue_reader():
    """Takes a process object from the queue as appended by the producer,
	appends it to a local list and iterates over the list, printing out 
	the results. Also removes terminated processes with p.poll()!=None
    """

    procs=[]
    while True:
	try:
            new_p=q.get(block=False)
            procs.append(new_p)
        except Queue.Empty:
            pass
#	sys.stderr.write('Number of current processes are '+str(len(procs)))
        for p in procs:
            printer(p)
            #printr=Thread(target=printer, args=[p])
            #printr.start()
#           sys.stdout.flush()
        procs=[]#p for p in procs if p.poll()==None]
        time.sleep(collection_interval)


def queue_writer(proc_names,no_trace, executable_location = "/cvmfs/softdrive.nl/apmechev/procfsamp/bin/Debug/procfs-sampler"):
    """Adds objects to the queue whenever a new process
       that matches any of the proc_names launches.
    """
    while True:
        time.sleep(collection_interval)
        find_pids=dict(zip(proc_names[:],[[] for x in range(len(proc_names))]))
	runtime_dir=os.environ['RUNDIR']
        with open(runtime_dir+'/pipeline_status','r') as step_file: ##Direct path???
            stepname=step_file.readline().strip('\n')
     
        for pname in proc_names:
            p_tmp=getPIDs(pname) #for all matching pids, trace ones not in notrace, add them
            for pid in p_tmp:
                if pid not in no_trace[pname]:
                    try:
                        proc_trace=subprocess.Popen([executable_location ,'--pid', pid,'--metric',stepname],stdout=subprocess.PIPE)
                        no_trace[pname].append(pid)
#                        sys.stderr.write(str(q.qsize())+' is length of the queue')
                        q.put(proc_trace)
                    except OSError:
                           sys.stderr.write("Launch error a@ proc Sampler")




def main(proc_names=["NDPPP","bbs-reducer", "losoto","ppworker"]):
    """main loop waiting for process"""

    collection_interval = DEFAULT_COLLECTION_INTERVAL
    if(proc_conf):
        config = proc_conf.get_config()
        collection_interval = config['collection_interval']
        proc_names.extend(config['proclist'])#Appends!
        pipeline_step_file = config['pipeline_step_file']
#    sys.stderr.write(str(proc_names))
    utils.drop_privileges()

    

    no_trace=dict(zip(proc_names[:],["" for x in range(len(proc_names))]))
        #zip each procname with "" and make dict
    for pname in proc_names:
        no_trace[pname] = getPIDs(pname) #This list initally holds all the already running processes
                                       #So that they don't get traced
    prod = Thread(target=queue_writer, args=[proc_names,no_trace])
    prod.daemon = True
    prod.start()
    sys.stderr.write(" starting consumer")

    cons = Thread(target=queue_reader)                           
    cons.daemon = True
    cons.start()
    while True:
        time.sleep(100)
#        sys.stderr.write("tracing "+str(len(procs))) 


if __name__ == "__main__":
    main()
