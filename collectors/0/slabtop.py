#!/usr/bin/env python
# This file is part of tcollector.
# Copyright (C) 2012  The tcollector Authors.
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.  This program is distributed in the hope that it
# will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
# General Public License for more details.  You should have received a copy
# of the GNU Lesser General Public License along with this program.  If not,
# see <http://www.gnu.org/licenses/>.
#

'''
slabtop parser. Requires either root priviliges or access to /proc/slabinfo
'''

import errno
import sys
import time
import subprocess
import re
import signal
import os

from collectors.lib import utils

try:
    from collectors.etc import slabtop_conf
except ImportError:
    slabtop_conf = None

DEFAULT_COLLECTION_INTERVAL=5

signal_received = None
def handlesignal(signum, stack):
    global signal_received
    signal_received = signum

def main():
    """top main loop"""

    collection_interval=DEFAULT_COLLECTION_INTERVAL

    if(slabtop_conf):
        config = slabtop_conf.get_config()
        collection_interval=config['collection_interval']

    global signal_received

    signal.signal(signal.SIGTERM, handlesignal)
    signal.signal(signal.SIGINT, handlesignal)

    while True:
        try:
            p_gstat = subprocess.Popen(
                ["slabtop", "-o", "-s", "c"],
                stdout=subprocess.PIPE,
            )
        except OSError, e:
            if e.errno == errno.ENOENT:
                # it makes no sense to run this collector here
                sys.exit(13) # we signal tcollector to not run us
            raise
    
        timestamp = 0
        timestamp = int(time.time())
        line = p_gstat.stdout.readline()
        fields=line.split()
        print "slab.Active %s %s " % (timestamp, fields[7])
        print "slab.Total %s %s " % (timestamp, fields[7])
        line = p_gstat.stdout.readline()
        line = p_gstat.stdout.readline()
        line = p_gstat.stdout.readline()
        fields=line.split()
        print "slab.Active %s %s " % (timestamp, fields[7])
        print "slab.Total_size %s %s " % (timestamp, fields[7])
        line = p_gstat.stdout.readline()
        line = p_gstat.stdout.readline()
        line = p_gstat.stdout.readline()


        if not line:
            # end of the program, die
            break
        for i in range(1,10):
            line = p_gstat.stdout.readline()
            timestamp = int(time.time())
            fields = line.split()
            print "slab.%s.size %s %s" % (fields[-1],timestamp, float(fields[-2][0:-1])*1000) 
            print "slab.%s.objects %s %s" % (fields[-1],timestamp, fields[0])
            print "slab.%s.use %s %s" % (fields[-1],timestamp, float(fields[2][:-1])/100)   
            
    
            sys.stdout.flush()
            
            if signal_received is None:
                signal_received = signal.SIGTERM
            try:
                os.kill(p_gstat.pid, signal_received)
            except Exception:
                pass
        p_gstat.wait()
        time.sleep(collection_interval)


if __name__ == "__main__":
    main()
