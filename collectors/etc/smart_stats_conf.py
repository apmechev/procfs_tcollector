#!/usr/bin/env python

def get_config():

    config = {
        'collection_interval': 20    # Seconds, how often to collect metric dat
    }

    return config
