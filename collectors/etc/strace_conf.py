#!/usr/bin/env python

def get_config():

    config = {
        'collection_interval': 10,    	# Seconds, how often to run strace
	'collection_duration':1,	# How long to run stace for (in seconds, min=1)
	'proclist':['NDPPP','bbs-reducer','losoto','ppworker','executable_args']
    }

    return config
