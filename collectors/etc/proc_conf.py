#!/usr/bin/env python
import json

def get_config(config_file='proc_collector.cfg'):

    with open(config_file,'rb') as json_data_file:
            config = json.safe_load(json_data_file) 

    return config
