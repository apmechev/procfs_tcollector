import os
import subprocess

def compile_procfs():
    os.chdir('procfsamp')
    mk=subprocess.Popen(['make','all'],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    mk.communicate()
    os.chdir('..')
    if os.path.exists('procfsamp/bin/Debug/strace-sampler') and os.path.exists('procfsamp/bin/Debug/procfs-sampler'):
        return True
    return False

def check_procfs():
    pass

def get_submodules():
    subs=subprocess.Popen(['git','submodule','init'])
    subs.communicate()
    subsup=subprocess.Popen(['git','submodule','update'])
    subsup.communicate()


def main():
    get_submodules()
    compile_procfs()


if __name__=='__main__':
    main()
